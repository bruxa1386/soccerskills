const nodemailer = require("nodemailer");
const { getFamilyForIdPlayer } = require("./repositories/family");
const { getForId } = require("./repositories/players");

const registerMail = (req, res) => {
  let email = "";
  if (req.body.email_familiar) {
    email = req.body.email_familiar;
  } else {
    email = req.body.email;
  }
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.USERGMAIL,
      pass: process.env.PASSWORDGMAIL,
    },
  });
  const mensaje = `${req.body.nombre} ${req.body.apellidos} Bienvenido a SoccerSkills.es! Ya estás registrado, empieza a disfrutar`;
  const mailOptions = {
    from: process.env.USERGMAIL,
    to: email,
    subject: "Registro en SoccerSkills.es",
    text: mensaje,
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email enviado: " + info.response);
    }
  });
};

const contractMail = async (req, res) => {
  let player = await getForId(req.body.id_jugador);
  if (!player.email) {
    player = await getFamilyForIdPlayer(player.id);
  }
  const email = player.email;
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.USERGMAIL,
      pass: process.env.PASSWORDGMAIL,
    },
  });
  const mensaje = `Hola ${player.nombre} ${player.apellidos}, has recibido un nuevo contrato! Entra a SoccerSkills para más información.`;

  const mailOptions = {
    from: process.env.USERGMAIL,
    to: email,
    subject: "Nuevo contrato en SoccerSkills.es",
    text: mensaje,
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email enviado: " + info.response);
    }
  });
};

module.exports = { registerMail, contractMail };
