import "./BotoneraVideo.css";
import { Link } from "react-router-dom";

const BotoneraVideo = ({
  activeVideo,
  setActiveVideo,
  videosList,
  setVideosList,
}) => {
  const nextVideo = () => {
    videosList.push(activeVideo);
    setActiveVideo(videosList.shift());
    setVideosList(videosList);
  };
  const prevVideo = () => {
    videosList.unshift(activeVideo);
    setActiveVideo(videosList.pop());
    setVideosList(videosList);
  };

  return (
    <div className="botonera-video">
      <Link
        to={`/player/${activeVideo.id_jugador}`}
        className="descripcion-botonera"
      >
        {activeVideo.nombre} {activeVideo.apellidos}: {activeVideo.descripcion},{" "}
        {activeVideo.edad} años
      </Link>
      <div>
        <button className="anterior-botonera" onClick={prevVideo}>
          ◄
        </button>
        <button className="siguiente-botonera" onClick={nextVideo}>
          ►
        </button>
      </div>
    </div>
  );
};

export default BotoneraVideo;
