import { useUser } from "../UserContext";
import "./VideosJugador.css";
import useFetch from "../useFetch";
import { Link } from "react-router-dom";
import PlayerCard from "../users/PlayerCard";
import "./FavScout.css";

const FavScout = () => {
  const [user] = useUser();

  const fav = useFetch("http://localhost:3000/api/scout/fav");

  return (
    <div>
      <h3>Jugadores Favoritos:</h3>
      <div className="fav-scout-profile">
        {user && fav && user.role === "scout" && user.id === fav.id_jugador && (
          <div>
            <Link to="/players">
              <button>+</button>
            </Link>
            <p>Añadir vídeo</p>
          </div>
        )}
        {fav && fav.map((jugador) => <PlayerCard jugador={jugador} key={jugador.id}/>)}
      </div>
    </div>
  );
};

export default FavScout;
