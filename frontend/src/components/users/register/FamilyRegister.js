import { useState } from "react";
import { useHistory } from "react-router";
import "./Register.css";

const FamilyRegister = () => {
  const [info, setInfo] = useState({});
  const [error, setError] = useState(false);
  const history = useHistory();
  const handleInput = (e) =>
    setInfo({ ...info, [e.target.name]: e.target.value });

  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3000/api/createfamily", {
      method: "POST",
      body: JSON.stringify(info),
      headers: { "Content-Type": "application/json" },
    });
    const data = await res.json();
    if (res.ok) {
      history.push("/");
    } else {
      const e = data.error;
      setError(e);
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit} className="family-form">
        <fieldset>
          <legend>Familiar/representante</legend>
          <label>
            Nombre:
            <input
              onChange={handleInput}
              name="nombre_familiar"
              maxlength="20"
              value={info.nombre_familiar}
              required
            ></input>
          </label>
          <label>
            Apellidos:
            <input
              onChange={handleInput}
              name="apellidos_familiar"
              maxlength="100"
              value={info.apellidos_familiar}
              required
            ></input>
          </label>
          <label>
            Dirección:
            <input
              onChange={handleInput}
              name="direccion_familiar"
              maxlength="120"
              value={info.direccion_familiar}
            ></input>
          </label>
          <label>
            DNI:
            <input
              onChange={handleInput}
              name="dni_familiar"
              maxlength="15"
              value={info.dni_familiar}
              required
            ></input>
          </label>
          <label>
            Email:
            <input
              onChange={handleInput}
              type="email"
              name="email_familiar"
              value={info.email_familiar}
              required
            ></input>
          </label>
          <label>
            Teléfono:
            <input
              onChange={handleInput}
              name="telefono_familiar"
              value={info.telefono_familiar}
            ></input>
          </label>
        </fieldset>

        <fieldset>
          <legend>Jugador </legend>
          <label>
            Nombre:
            <input
              onChange={handleInput}
              name="nombre"
              maxlength="20"
              value={info.nombre}
              required
            ></input>
          </label>
          <label>
            Apellidos:
            <input
              onChange={handleInput}
              name="apellidos"
              maxlength="100"
              value={info.apellidos}
              required
            ></input>
          </label>
          <br />
          <label>
            Username:
            <input
              onChange={handleInput}
              name="username"
              maxlength="20"
              value={info.username}
              required
            ></input>
          </label>
          <label>
            Password:
            <input
              onChange={handleInput}
              type="password"
              maxlength="40"
              minlength="5"
              name="password"
              value={info.password}
              required
            ></input>
          </label>
          <br />
          <label>
            Fecha de nacimiento:
            <input
              type="date"
              onChange={handleInput}
              name="fecha_nacimiento"
              value={info.fecha_nacimiento}
              required
            />
          </label>
          <label>
            Dirección:
            <input
              onChange={handleInput}
              name="direccion"
              maxlength="120"
              value={info.direccion}
            ></input>
          </label>
          <br />
          <label>
            DNI:
            <input
              onChange={handleInput}
              name="dni"
              maxlength="15"
              value={info.dni}
            ></input>
          </label>
          <label>
            Email:
            <input
              onChange={handleInput}
              type="email"
              name="email"
              value={info.email}
            ></input>
          </label>
          <br />
          <label>
            Teléfono:
            <input
              onChange={handleInput}
              name="telefono"
              value={info.telefono}
            ></input>
          </label>
          <label>
            Equipo:
            <input
              onChange={handleInput}
              name="equipo"
              maxlength="40"
              value={info.equipo}
            ></input>
          </label>
          <br />
          <label>
            Pierna:
            <select onChange={handleInput} name="pierna" value={info.pierna}>
              <option value="">NS/NC</option>
              <option value="diestra">Diestra</option>
              <option value="zurda">Zurda</option>
              <option value="ambas">Ambas</option>
            </select>
          </label>
          <label>
            Posición:
            <select
              onChange={handleInput}
              name="posicion"
              value={info.posicion}
              required
            >
              <option value="portero">Portero</option>
              <option value="defensa">Defensa</option>
              <option value="medio">Medio</option>
              <option value="delantero">Delantero</option>
            </select>
          </label>
          <br />
          <button>Regístrate</button>
        </fieldset>
      </form>
      {error && <div className="error-form">{error}</div>}
    </div>
  );
};

export default FamilyRegister;
