const {
  createContrat,
  modificateContratState,
  searchContratId,
} = require("../repositories/contrats");

const newContrat = async (req, res, next) => {
  try {
    await createContrat(req.body);
    res.status(201);
    res.send(req.body);
    next();
  } catch (err) {
    res.status(500);
    res.send({ error: err.message });
  }
};

const acceptOrRejectContract = async (req, res) => {
  try {
    await modificateContratState(req.params.id, req.body.estado, req.auth.id);
    res.status(201);
    res.send("Contrato modificado");
  } catch (err) {
    res.status(500);
    res.send({ error: err.message });
  }
};

const contractForId = async (req, res) => {
  try {
    const contract = await searchContratId(req.params.id);
    res.status(201);
    res.send(contract[0]);
  } catch (err) {
    res.status(500);
    res.send({ error: err.message });
  }
};
module.exports = { newContrat, acceptOrRejectContract, contractForId };
