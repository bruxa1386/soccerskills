const mysql = require('mysql2/promise')
const { getConnection } = require('../database')

const getScoreVideos = async () => {
    const connection = await getConnection()
    const [rows] = await connection.query(`SELECT AVG(puntuacion) AS media, id_video, id_jugador FROM puntuaciones P LEFT JOIN videos V ON V.id = P.id_video GROUP BY id_video ORDER BY media DESC;`);
    return rows
}

const allVideos = async () => {
    const connection = await getConnection()
    const [rows] = await connection.query(`SELECT V.id, V.video, V.descripcion, V.id_jugador, J.nombre, J.apellidos, J.equipo,
     TIMESTAMPDIFF(YEAR,J.fecha_nacimiento,CURDATE()) AS edad, J.id as id_jugador
    FROM videos V LEFT JOIN jugadores J ON V.id_jugador = J.id;`)
    return rows
}

module.exports = { getScoreVideos, allVideos }