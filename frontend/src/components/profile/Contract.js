import { useParams, useHistory } from "react-router-dom";
import useFetch from "../useFetch";
import "./Contract.css";
import moment from "moment";
import { useUser } from "../UserContext";

const Contract = () => {
  //const [info, setInfo] = useState({ estado: "" });
  const history = useHistory();
  const [user] = useUser();
  const token = user && user.token;
  const { id } = useParams();
  const contrato = useFetch(`http://localhost:3000/api/contract/${id}`);
  const {
    nombre,
    apellidos,
    equipo,
    email,
    salario,
    clausula,
    tiempo_contrato,
    fecha_inicio,
    imagen,
    estado,
    id_jugador,
    id_ojeador,
    nombre_jugador,
    apellidos_jugador,
    email_jugador,
    equipo_jugador,
    email_familia,
  } = contrato;
  moment.locale("es");
  const fecha = moment(fecha_inicio).format("DD MMM YY");

  const sendResponse = async (info) => {
    await fetch(`http://localhost:3000/api/response/contrat/${id}`, {
      method: "PUT",
      body: JSON.stringify(info),
      headers: {
        "Content-type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    history.push(`/profile`);
  };

  return (
    <div className="contract-info-container">
      <div className="contract-info-contract">
        {(user.role === "player" || user.role === "represented") && (
          <div>
            <div
              className="avatar"
              style={{ backgroundImage: `url(${imagen})` }}
            />
            <p>
              Ojeador: {nombre} {apellidos}
            </p>
            <p>Equipo: {equipo}</p>
            <p>email: {email} </p>
          </div>
        )}
        {user.role === "scout" && (
          <div>
            <div
              className="avatar"
              style={{ backgroundImage: `url(${imagen})` }}
            />
            <p>
              Jugador: {nombre_jugador} {apellidos_jugador}
            </p>
            <p>
              Equipo: {equipo_jugador ? equipo_jugador : "Jugador sin equipo"}
            </p>
            <p>email: {email_jugador ? email_jugador : email_familia}</p>
          </div>
        )}
        <div>
          <p>estado: {estado}</p>
          <p>sueldo: {salario}€/temporada </p>
          <p>temporadas: {tiempo_contrato} </p>
          <p>inicio: {fecha} </p>
          <p>clausula: {clausula}€ </p>
        </div>
      </div>
      {(user.role === "player" || user.role === "represented") &&
        user.id === id_jugador &&
        estado === "solicitado" && (
          <div>
            <button onClick={() => sendResponse({ estado: "confirmado" })}>
              Aceptar
            </button>
            <button onClick={() => sendResponse({ estado: "rechazado" })}>
              Rechazar
            </button>
          </div>
        )}
      {(((user.role === "player" || user.role === "represented") &&
        user.id === id_jugador &&
        estado === "confirmado") ||
        (user.role === "scout" && user.id === id_ojeador)) &&
        (estado === "confirmado" || estado === "solicitado") && (
          <div>
            <button onClick={() => sendResponse({ estado: "cancelado" })}>
              Cancelar contrato
            </button>
          </div>
        )}
    </div>
  );
};

export default Contract;
