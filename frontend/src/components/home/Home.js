import { useUser } from '../UserContext'
import RegisterInfo from '../users/register/RegisterInfo'
import './Home.css'

const Home = () => {
    const [user] = useUser()

    return (
        <>
            {!user &&
                <div className="home">
                    <div className="start-info">
                        <p>Date de alta como jugador, familiar/representante u ojeador</p>
                        <p>Comparte vídeos demostrando tus habilidades</p>
                        <p>Interactúa con otros jugadores y aprende de los demás</p>
                        <p>Consigue un contrato como futbolista</p>
                    </div>
                    <RegisterInfo />
                </div>
            }
        </>
    )
}


export default Home