import { format } from "date-fns";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useUser } from "../UserContext";

const NewContract = () => {
  const [info, setInfo] = useState({});
  const [error, setError] = useState(false);
  const history = useHistory();
  const { id } = useParams();
  const [user] = useUser();

  const token = user && user.token;

  const today = new Date();
  const minAllowedDate = format(today, "yyyy-MM-dd");

  const handleInput = (e) =>
    setInfo({ ...info, [e.target.name]: e.target.value });

  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch(`http://localhost:3000/api/scout/player/${id}`, {
      method: "POST",
      body: JSON.stringify(info),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    const data = await res.json();
    if (res.ok) {
      history.push("/players");
    } else {
      const e = data.error;
      setError(e);
    }
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>Nuevo Contrato</legend>
          <label>
            Tiempo del contrato:
            <select
              onChange={handleInput}
              name="tiempo_contrato"
              value={info.tiempo_contrato}
            >
              <option value=""></option>
              <option value="1 temporada">1 temporada</option>
              <option value="2 temporadas">2 temporadas</option>
              <option value="3 temporadas">3 temporadas</option>
              <option value="4 temporadas">4 temporadas</option>
              <option value="5 temporadas">5 temporadas</option>
            </select>
          </label>
          <br />
          <label>
            Claúsula de rescisión:
            <input
              onChange={handleInput}
              name="clausula"
              maxlength="15"
              value={info.clausula}
            ></input>
            €
          </label>
          <br />
          <label>
            Fecha de inicio:
            <input
              type="date"
              onChange={handleInput}
              name="fecha_inicio"
              min={minAllowedDate}
              value={info.fecha_inicio}
              required
            />
          </label>
          <label>
            Salario:
            <input
              onChange={handleInput}
              name="salario"
              maxlength="15"
              value={info.salario}
            ></input>
            €/temporada
          </label>
          <input type="submit" value="Ofrecer contrato"></input>
        </fieldset>
      </form>
      {error && <div>{error}</div>}
    </div>
  );
};

export default NewContract;
