import "./App.css";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import { Route, Switch } from "react-router-dom";
import Home from "./components/home/Home";
import Login from "./components/login/Login";
import Players from "./components/users/Players";
import Videos from "./components/video/Videos";
import FamilyRegister from "./components/users/register/FamilyRegister";
import ScoutRegister from "./components/users/register/ScoutRegister";
import PlayerRegister from "./components/users/register/PlayerRegister";
import Profile from "./components/profile/Profile";
import UploadVideo from "./components/profile/UploadVideo";
import UploadImage from "./components/profile/UploadImage";
import Playersprofile from "./components/profile/PlayersProfile";
import NewContract from "./components/users/NewContract";
import Contract from "./components/profile/Contract";
import InfoChange from "./components/users/register/InfoChange";

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/uploadVideo">
          <UploadVideo />
        </Route>
        <Route path="/uploadImage">
          <UploadImage />
        </Route>
        <Route path="/players">
          <Players />
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/newcontract/:id" exact>
          <NewContract />
        </Route>
        <Route path="/contract/:id" exact>
          <Contract />
        </Route>
        <Route path="/player/register" exact>
          <PlayerRegister />
        </Route>
        <Route path="/player/:id" exact>
          <Playersprofile />
        </Route>
        <Route path="/family/register" exact>
          <FamilyRegister />
        </Route>
        <Route path="/scout/register">
          <ScoutRegister />
        </Route>
        <Route path="/infoChange">
          <InfoChange />
        </Route>
        <Route path="/videos/:id?" exact>
          <Videos />
        </Route>
        <Route path="/" exact>
          <Home />
        </Route>
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
