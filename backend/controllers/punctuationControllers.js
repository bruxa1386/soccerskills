const { addPunctuation } = require("../repositories/punctuations");

const scoutVoteVideo = async (req, res) => {
  try {
    const puntuacion = await addPunctuation(
      req.body.puntuacion,
      req.body.favorito,
      req.params.id,
      req.auth.id
    );
    res.status(200);
    res.send(puntuacion);
  } catch (err) {
    res.send({ error: err.message });
  }
};

module.exports = { scoutVoteVideo };
