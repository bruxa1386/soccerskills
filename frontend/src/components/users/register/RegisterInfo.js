import { Link } from "react-router-dom";

const RegisterInfo = () => {
  return (
    <div className="register-info">
      <div className="resgister-player-info">
        <p>
          Si eres futbolista mayor de edad <br />
          <Link to="/player/register">☛ Regístrate aquí</Link>
        </p>
      </div>
      <br />
      <div className="resgister-scout-info">
        <p>
          Si eres ojeador <br />
          <Link to="/scout/register">☛ Regístrate aquí</Link>
        </p>
      </div>
      <br />
      <div className="resgister-family-info">
        <p>
          Si eres familiar o representante de un jugador <br />
          <Link to="/family/register">☛ Regístrate aquí</Link>
        </p>
      </div>
    </div>
  );
};

export default RegisterInfo;
