const { getConnection } = require("../database")


const postNewFamily = async (user) => {

    const connection = await getConnection()
    const query = (`INSERT INTO familias( nombre, apellidos,
         direccion, dni, email, telefono)
        VALUES(?, ?, ?, ?, ?, ?);`)

    await connection.query(query, [user.nombre_familiar,
    user.apellidos_familiar, user.direccion_familiar, user.dni_familiar,
    user.email_familiar, user.telefono_familiar])

}

const familyForDni = async (dni) => {
    const connection = await getConnection()
    const [rows] = await connection.query(`SELECT * FROM familias
    WHERE dni = '${dni}';`)

    return rows[0]
}


const getFamilyForIdPlayer = async (id) => {
    const connection = await getConnection()
    const query = (`SELECT F.*
    FROM familias F LEFT JOIN jugadores J ON J.id_familia= F.id
    WHERE J.id= ?;`)
    const [rows] = await connection.query(query, [id])
    return rows[0]
}

const deleteFamilyForId = async (id) => {
    const connection = await getConnection()
    await connection.query(`DELETE FROM familias WHERE id = ${id}`)
}

module.exports = { postNewFamily, familyForDni, deleteFamilyForId, getFamilyForIdPlayer }