const Joi = require("joi");

const checkLogin = (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().max(20).required(),
    password: Joi.string().min(5).max(40).required(),
  });
  const validation = schema.validate(req.body);
  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

const checkID = (req, res, next) => {
  const schema = Joi.object({
    id: Joi.number().required(),
  });
  const validation = schema.validate(req.params);
  if (validation.error) {
    res.status(400);
    throw new Error("El id debe ser un número");
  } else {
    next();
  }
};

const checkPassword = (req, res, next) => {
  const schema = Joi.object({
    password: Joi.string().min(5).max(40).required(),
  });
  const validation = schema.validate(req.body);
  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

module.exports = { checkLogin, checkID, checkPassword };
