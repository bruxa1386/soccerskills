import { useUser } from "../UserContext";
import "./VideosJugador.css";
import useFetch from "../useFetch";
import { Link } from "react-router-dom";

const VideosJugador = () => {
  const [user] = useUser();
  const token = user && user.token;

  const videos = useFetch("http://localhost:3000/api/player/videos");
  const handleClick = async (id) => {
    const result = window.confirm(`Seguro que quieres eliminar este video?`);
    if (result) {
      await fetch(`http://localhost:3000/api/player/delete/video/${id}`, {
        method: "DELETE",
        headers: {
          Authorization: "Bearer " + token,
        },
      });
      window.location.reload();
    }
  };
  return (
    <div className="videos-jugador-profile">
      {user && (user.role === "player" || user.role === "represented") && (
        <div>
          <Link to="/uploadVideo">
            <button className="boton-añadir-video">+</button>
          </Link>
          <p>Añadir vídeo</p>
        </div>
      )}
      {videos &&
        videos.map((video) => (
          <div key={video.id}>
            <Link to={`/videos/${video.id}`} className="video-jugador-link">
              <img
                src={`https://img.youtube.com/vi/${video.video}/hqdefault.jpg`}
                alt="video"
              />
            </Link>
            <div className="video-jugador-button">
              <p>
                <button
                  id="boton-eliminar-video"
                  onClick={() => handleClick(video.id)}
                >
                  X
                </button>
                {video.descripcion}
              </p>
            </div>
          </div>
        ))}
    </div>
  );
};

export default VideosJugador;
