import { Link } from "react-router-dom";
import useFetch from "../useFetch";

const PlayerCard = ({ jugador }) => {
  const {
    id,
    username,
    imagen,
    nombre,
    apellidos,
    edad,
    posicion,
    pierna,
    equipo,
    id_jugador,
  } = jugador;
  const videos = useFetch(`http://localhost:3000/api/videos/${id_jugador}`);
  return (
    <div className="player-card" key={username}>
      <div className="player-card-info">
        <Link
          to={`/player/${id_jugador}`}
          className="image"
          style={{ backgroundImage: `url(${imagen})` }}
        />
        <Link to={`/player/${id_jugador}`} className="datos-personales">
          <p>Nombre: {nombre}</p>
          <p>Apellidos: {apellidos}</p>
          <p>Edad: {edad}</p>
        </Link>
      </div>
      <Link to={`/player/${id_jugador}`} className="datos-jugador">
        <p>{posicion}</p>
        <p>{pierna}</p>
        <p>{equipo}</p>
      </Link>
      <div className="videos-jugador">
        <h3>Vídeos:</h3>
        <div className="video-container-playercard">
          {videos &&
            videos.map((v) => (
              <Link to={`/videos/${v.id}`} className="image-video" key={v.id}>
                <img
                  src={`https://img.youtube.com/vi/${v.video}/hqdefault.jpg`}
                  alt="video"
                />
              </Link>
            ))}
        </div>
      </div>
    </div>
  );
};

export default PlayerCard;
