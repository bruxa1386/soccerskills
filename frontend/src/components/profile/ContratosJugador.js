import "./ContratosJugador.css";
import useFetch from "../useFetch";
import ContractCard from "./ContractCard";

const ContratosJugador = () => {
  const contratos = useFetch("http://localhost:3000/api/player/contracts");
  return (
    <div className="contratos-jugador-profile">
      {contratos &&
        contratos.map((contract) => (
          <div key={contract.id}>
            <ContractCard contract={contract} />
          </div>
        ))}
    </div>
  );
};

export default ContratosJugador;
