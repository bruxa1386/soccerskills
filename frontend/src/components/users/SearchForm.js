import { useEffect, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";

const SearchForm = ({ setJugadores }) => {
  const [nombre, setNombre] = useState("");
  const [apellidos, setApellidos] = useState("");
  const [equipo, setEquipo] = useState("");
  const [pierna, setPierna] = useState("");
  const [posicion, setPosicion] = useState("");
  const [edadMin, setEdadMin] = useState("");
  const [edadMax, setEdadMax] = useState("");
  const [order, setOrder] = useState("");
  const [direction, setDirection] = useState("");
  const query = useLocation().search;
  const history = useHistory();
  useEffect(() => {
    const cargarJugadores = async () => {
      const res = await fetch(
        `http://localhost:3000/api/players/explore${query}`
      );
      const jugadores = await res.json();
      setJugadores(jugadores);
    };
    cargarJugadores();
  }, [query, setJugadores]);

  const filtrarJugadores = (e) => {
    e.preventDefault();

    let newQuery = "/players?";
    if (nombre) {
      newQuery += `nombre=${nombre}&`;
    }
    if (apellidos) {
      newQuery += `apellidos=${apellidos}&`;
    }
    if (equipo) {
      newQuery += `equipo=${equipo}&`;
    }
    if (pierna) {
      newQuery += `pierna=${pierna}&`;
    }
    if (posicion) {
      newQuery += `posicion=${posicion}&`;
    }
    if (edadMin) {
      newQuery += `edadMin=${edadMin}&`;
    }
    if (edadMax) {
      newQuery += `edadMax=${edadMax}&`;
    }
    if (order) {
      newQuery += `order=${order}&`;
    }
    if (direction) {
      newQuery += `direction=${direction}&`;
    }
    history.push(newQuery);
  };

  return (
    <form onSubmit={filtrarJugadores} className="searchform">
      <fieldset>
        <legend>Filtro de jugadores</legend>
        <input
          placeholder="Nombre..."
          value={nombre}
          onChange={(e) => setNombre(e.target.value)}
        />
        <br />
        <input
          placeholder="Apellidos..."
          value={apellidos}
          onChange={(e) => setApellidos(e.target.value)}
        />
        <br />
        <input
          placeholder="Equipo..."
          value={equipo}
          onChange={(e) => setEquipo(e.target.value)}
        />
        <br />
        <input
          placeholder="Edad mínima..."
          value={edadMin}
          type="number"
          max="99"
          onChange={(e) => setEdadMin(e.target.value)}
        />
        <br />
        <input
          placeholder="Edad máxima..."
          value={edadMax}
          type="number"
          min="1"
          onChange={(e) => setEdadMax(e.target.value)}
        />
        <br />
        <select onChange={(e) => setPierna(e.target.value)} value={pierna}>
          <option value="">Pierna</option>
          <option value="diestra">Diestra</option>
          <option value="zurda">Zurda</option>
          <option value="ambas">Ambas</option>
        </select>
        <select onChange={(e) => setPosicion(e.target.value)} value={posicion}>
          <option value="">Posición</option>
          <option value="portero">Portero</option>
          <option value="defensa">Defensa</option>
          <option value="medio">Medio</option>
          <option value="delantero">Delantero</option>
        </select>
        <br />
        <select onChange={(e) => setOrder(e.target.value)} value={order}>
          <option value="">Ordenar por...</option>
          <option value="edad">Edad</option>
          <option value="nombre">Nombre</option>
          <option value="apellidos">Apellidos</option>
          <option value="equipo">Equipo</option>
          <option value="pierna">Pierna</option>
          <option value="posicion">Posicion</option>
        </select>
        <select
          onChange={(e) => setDirection(e.target.value)}
          value={direction}
        >
          <option value="">Dirección...</option>
          <option value="asc">Ascendente</option>
          <option value="desc">Descendente</option>
        </select>
        <br />
        <button type="submit" value="Buscar">Buscar</button>
      </fieldset>
    </form>
  );
};

export default SearchForm;
