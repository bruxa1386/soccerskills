import "./Videos.css";
import { useParams, Link } from "react-router-dom";
import BotoneraVideo from "./BotoneraVideo";
import { useEffect, useState } from "react";

const Videos = () => {
  const { id } = useParams();
  const [activeVideo, setActiveVideo] = useState({});
  const [videosList, setVideosList] = useState([]);
  useEffect(() => {
    const fetchVideos = async () => {
      const res = await fetch(`http://localhost:3000/api/videos`);
      const videos = await res.json();
      setActiveVideo(
        id ? videos.filter((video) => video.id === +id)[0] : videos[0]
      );
      setVideosList(videos.filter((video) => video.id !== +id));
    };
    fetchVideos();
  }, []);

  const setClickedVideoToActive = (videoId) => {
    const videoIndex = videosList.findIndex((video) => video.id === videoId);
    const tempVideo = videosList[videoIndex];
    videosList[videoIndex] = activeVideo;
    setActiveVideo(tempVideo);
    setVideosList(videosList);
  };
  if (!activeVideo) {
    return <div>Este video no existe!!</div>;
  }
  return (
    <div className="video-container">
      <div className="video-activo">
        {activeVideo && (
          <div className="video">
            <iframe
              src={"https://www.youtube.com/embed/" + activeVideo.video}
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay;
                clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
            <BotoneraVideo
              activeVideo={activeVideo}
              setActiveVideo={setActiveVideo}
              videosList={videosList}
              setVideosList={setVideosList}
            />
          </div>
        )}
      </div>
      <div className="video-list">
        {videosList &&
          videosList.map((v) => (
            <div
              className="video-link"
              key={v.id}
              onClick={() => {
                setClickedVideoToActive(v.id);
              }}
            >
              <div className="videopreview">
                <img
                  src={`https://img.youtube.com/vi/${v.video}/hqdefault.jpg`}
                  alt="video"
                />
              </div>
              <div className="info-videolist">
                <p>
                  {v.nombre} {v.apellidos}
                  <br />
                  {v.edad} años
                  <br />
                  {v.descripcion}
                </p>
              </div>
              <hr />
            </div>
          ))}
      </div>
    </div>
  );
};

export default Videos;
