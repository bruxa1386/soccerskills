const Joi = require("joi");

const checkSendContract = (req, res, next) => {
  const today = new Date();
  req.body.id_ojeador = req.auth.id;
  req.body.id_jugador = req.params.id;
  const schema = Joi.object({
    id_ojeador: Joi.number().required(),
    id_jugador: Joi.number().required(),
    tiempo_contrato: Joi.string().valid(
      "1 temporada",
      "2 temporadas",
      "3 temporadas",
      "4 temporadas",
      "5 temporadas"
    ),
    clausula: Joi.number(),
    fecha_inicio: Joi.date().required().min(today),
    salario: Joi.number(),
  });
  const validation = schema.validate(req.body);
  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

module.exports = { checkSendContract };
