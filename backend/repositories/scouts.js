const mysql = require("mysql2/promise");
const { getConnection } = require("../database");

const scoutExists = async (username) => {
  const connection = await getConnection();
  const [rows] = await connection.query(`SELECT * FROM ojeadores
    WHERE username = '${username}';`);

  return rows[0] ? true : false;
};

const getAllScouts = async () => {
  const connection = await getConnection();
  const [rows] = await connection.query(`SELECT id, username, nombre, apellidos,
     equipo, imagen FROM ojeadores WHERE role = 'scout';`);
  return rows;
};

const postNewScout = async (user, password) => {
  const connection = await getConnection();
  const query = `INSERT INTO ojeadores(username, password, nombre, apellidos,
         dni, email, telefono, equipo)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?);`;
  await connection.query(query, [
    user.username,
    password,
    user.nombre,
    user.apellidos,
    user.dni,
    user.email,
    user.telefono,
    user.equipo,
  ]);
};

const getForUsernameScout = async (username) => {
  const connection = await getConnection();
  const [rows] = await connection.query(`SELECT username, nombre, apellidos,
    imagen, id, password, role, direccion, telefono, equipo, email FROM ojeadores
    WHERE username = '${username}';`);
  return rows[0];
};

const getForIdScout = async (id) => {
  const connection = await getConnection();
  const [rows] = await connection.query(`SELECT * FROM ojeadores
    WHERE id = '${id}';`);
  return rows[0];
};

const putInfoScout = async (user, auth) => {
  const connection = await getConnection();
  let data = [];
  if (!user.username) {
    data = [...data, auth.username];
  } else {
    data = [...data, user.username];
  }
  if (!user.direccion) {
    data = [...data, auth.direccion];
  } else {
    data = [...data, user.direccion];
  }
  if (!user.email) {
    data = [...data, auth.email];
  } else {
    data = [...data, user.email];
  }
  if (!user.telefono) {
    data = [...data, auth.telefono];
  } else {
    data = [...data, user.telefono];
  }
  if (!user.equipo) {
    data = [...data, auth.team];
  } else {
    data = [...data, user.equipo];
  }
  const query = `UPDATE ojeadores SET username = ?,
    direccion = ?, email = ?, telefono = ?, equipo = ?
    WHERE (id = ?)`;
  await connection.query(query, [...data, auth.id]);
};

const putPasswordScout = async (password, id) => {
  const connection = await getConnection();
  const query = `UPDATE ojeadores SET password = ? WHERE (id= ?)`;

  await connection.query(query, [password, id]);
};

const getFavScout = async (id) => {
  const connection = await getConnection();
  const query = `SELECT DISTINCT J.id, J.nombre, J.apellidos, J.imagen, J.pierna,
    J.posicion, J.equipo, TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad,
    JO.*
    FROM jugadores J LEFT JOIN jugadores_ojeadores JO ON J.id = JO.id_jugador
    LEFT JOIN ojeadores O ON O.id = JO.id_ojeador
    WHERE JO.favorito = 1 AND JO.id_ojeador = ?;`;
  const [rows] = await connection.query(query, [id]);
  return rows;
};

const newFavScout = async (id_player, id_scout) => {
  const connection = await getConnection();
  const query = `INSERT INTO jugadores_ojeadores (favorito, id_jugador, id_ojeador)
   VALUES ('1', ?, ?);`;
  await connection.query(query, [id_player, id_scout]);
};

const changeFavScout = async (info) => {
  const connection = await getConnection();
  const query = `UPDATE jugadores_ojeadores SET favorito = ? WHERE (id= ?);`;
  await connection.query(query, [info.favorito === 1 ? "0" : "1", info.id]);
};

const getFavScoutPlayer = async (req) => {
  const connection = await getConnection();
  const query = `SELECT * FROM jugadores_ojeadores WHERE id_jugador = ? AND id_ojeador = ?;`;
  const [rows] = await connection.query(query, [req.id_jugador, req.id]);
  return rows[0];
};

const postImageScout = async (image, id) => {
  const connection = await getConnection();
  const query = `UPDATE ojeadores SET imagen = ? WHERE (id= ?)`;
  await connection.query(query, [image, id]);
};

const getContratsScout = async (id) => {
  const connection = await getConnection();
  const query = `SELECT C.id, C.estado, C.tiempo_contrato, C.id_jugador, C.clausula, C.fecha_inicio,
    C.salario, O.equipo as equipo_ojeador,O.nombre as nombre_ojeador,O.apellidos as apellidos_ojeador,O.email as email_ojeador,
     J.imagen, J.nombre, J.apellidos, J.posicion, J.pierna,
    J.equipo as equipo_jugador, J.id as id_jugador, TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad
        FROM contratos C LEFT JOIN ojeadores O ON C.id_ojeador = O.id
        LEFT JOIN jugadores J ON C.id_jugador = J.id
        WHERE C.id_ojeador=? ORDER BY C.id DESC;`;
  const [rows] = await connection.query(query, [id]);
  return rows;
};

module.exports = {
  scoutExists,
  postNewScout,
  getForUsernameScout,
  putInfoScout,
  putPasswordScout,
  getFavScout,
  getForIdScout,
  postImageScout,
  getContratsScout,
  getFavScoutPlayer,
  newFavScout,
  changeFavScout,
  getAllScouts
};
