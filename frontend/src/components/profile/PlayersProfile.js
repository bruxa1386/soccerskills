import { Link, useHistory, useParams } from "react-router-dom";
import useFetch from "../useFetch";
import { useUser } from "../UserContext";
import "./PlayersProfile.css";

const Playersprofile = () => {
  const { id } = useParams();
  const jugador = useFetch(`http://localhost:3000/api/playerprofile/${id}`);
  const { imagen, nombre, apellidos, edad, equipo, posicion, pierna } = jugador;
  const videos = useFetch(`http://localhost:3000/api/player/videos/${id}`);
  const [user] = useUser();
  const history = useHistory();
  const token = user && user.token;
  let id_scout = 0;
  if (user && user.role === "scout") {
    id_scout = user.id;
  }

  const fav = useFetch(`http://localhost:3000/api/fav/${id}/scout/${id_scout}`);

  const handleClick = async () => {
    if (!fav) {
      const res = await fetch(
        `http://localhost:3000/api/scout/${id_scout}/player/${id}`,
        {
          method: "POST",
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      );
      window.location.reload();
    } else {
      const res = await fetch(`http://localhost:3000/api/scout/fav`, {
        method: "PUT",
        body: JSON.stringify(fav),
        headers: {
          "Content-type": "application/json",
          Authorization: "Bearer " + token,
        },
      });
      window.location.reload();
    }
  };

  return (
    <div className="player-card-profile-container">
      <div className="card-profile-player-info">
        <div
          to={`/player/${id}`}
          className="image-profile-card"
          style={{ backgroundImage: `url(${imagen})` }}
        />
        <div>
          <p>
            Nombre: {nombre} {apellidos}
          </p>
          <p>Edad: {edad} años</p>
          <p>Equipo: {equipo}</p>
          <p>Posicion: {posicion}</p>
          <p>Pierna: {pierna}</p>
        </div>
      </div>
      <div className="card-profile-video-container">
        {videos &&
          videos.map((video) => (
            <div key={video.id}>
              <Link
                to={`/videos/${video.id}`}
                className="video-jugador-link-profile"
              >
                <img
                  src={`https://img.youtube.com/vi/${video.video}/hqdefault.jpg`}
                  alt="video"
                />
              </Link>
              <p>{video.descripcion}</p>
            </div>
          ))}
      </div>
      {user && user.role === "scout" && (
        <div className="botonera-perfil">
          <button
            className="boton-contrato"
            onClick={() => history.push(`/newcontract/${id}`)}
          >
            Ofrecer contrato
          </button>
          <button className="boton-fav" onClick={handleClick}>
            {fav.favorito === 1 ? "★" : "✩"}
          </button>
        </div>
      )}
    </div>
  );
};

export default Playersprofile;
