const { delByUsername } = require("../repositories/admin");

const deleteUser = async (req, res) => {
  try {
    await delByUsername(req.body.username);
    res.status(202);
    res.send("El usuario ha sido borrado con éxito");
  } catch (err) {
    res.status(400);
    res.send({ error: err.message });
  }
};

module.exports = { deleteUser };
