import "./Players.css";
import { useUser } from "../UserContext";
import SearchForm from "./SearchForm";
import { useState } from "react";
import RegisterInfo from "./register/RegisterInfo";
import PlayerCard from "./PlayerCard";

const Players = () => {
  const [jugadores, setJugadores] = useState("");
  const [user] = useUser();
  return (
    <div className="players">
      <SearchForm setJugadores={setJugadores} />
      <div className="player-container">
        {jugadores &&
          jugadores.map((jugador) => <PlayerCard jugador={jugador} key={jugador.id}/>)}
      </div>
      {!user && <RegisterInfo />}
    </div>
  );
};

export default Players;
