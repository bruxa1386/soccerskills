const { getScoreVideos, allVideos } = require("../repositories/videos");

const scoreVideos = async (req, res) => {
  try {
    const users = await getScoreVideos();
    res.send(users);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const getAllVideos = async (req, res) => {
  try {
    const videos = await allVideos();
    res.send(videos);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

module.exports = { scoreVideos, getAllVideos };
