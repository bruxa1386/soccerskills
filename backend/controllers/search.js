const { getConnection } = require("../database");

const search = async (req, res, next) => {
  try {
    const connection = getConnection();
    const {
      nombre,
      apellidos,
      edadMin = 0,
      edadMax = 99,
      equipo,
      pierna,
      posicion,
      order,
      direction,
    } = req.query;
    let query = `SELECT id , id as id_jugador, username, nombre, apellidos, TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad,
        pierna, posicion, imagen, equipo FROM jugadores`;

    const orderDirection =
      (direction && direction.toLowerCase()) === "desc" ? "DESC" : "ASC";
    let orderBy;
    switch (order) {
      case "edad":
        orderBy = "edad";
        break;
      case "nombre":
        orderBy = "nombre";
        break;
      case "apellidos":
        orderBy = "apellidos";
        break;
      case "equipo":
        orderBy = "equipo";
        break;
      case "pierna":
        orderBy = "pierna";
        break;
      case "posicion":
        orderBy = "posicion";
        break;
      default:
        orderBy = "nombre";
    }
    if (
      nombre ||
      apellidos ||
      edadMin ||
      edadMax ||
      equipo ||
      pierna ||
      posicion
    ) {
      const conditions = [];
      const params = [];
      if (nombre) {
        conditions.push(`nombre LIKE ?`);
        params.push(`%${nombre}%`);
      }
      if (apellidos) {
        conditions.push(`apellidos LIKE ?`);
        params.push(`%${apellidos}%`);
      }
      if (edadMin || edadMax) {
        conditions.push(
          `TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) BETWEEN ? AND ?`
        );
        params.push(`${edadMin}`, `${edadMax}`);
      }
      if (equipo) {
        conditions.push(`equipo LIKE ?`);
        params.push(`%${equipo}%`);
      }
      if (pierna) {
        conditions.push(`pierna = ?`);
        params.push(`${pierna}`);
      }
      if (posicion) {
        conditions.push(`posicion = ?`);
        params.push(`${posicion}`);
      }
      query = `${query} WHERE ${conditions.join(
        ` AND `
      )} ORDER BY ${orderBy} ${orderDirection}`;
      const [result] = await (await connection).query(query, params);
      res.send(result);
    }
  } catch (err) {
    next(err);
  }
};

module.exports = search;
