import { useState } from "react";
import { useUser } from "../../UserContext";

const InfoChange = () => {
  const [info, setInfo] = useState({});
  const [error, setError] = useState(false);
  const [user] = useUser();
  const token = user && user.token;

  const handleInput = (e) =>
    setInfo({
      ...info,
      [e.target.name]: e.target.value,
    });

  const handleSubmit = async (e) => {
    e.preventDefault();

    let url = "";
    if (user.role === "scout") {
      url = "http://localhost:3000/api/scout";
    } else if (user.role === "player") {
      url = "http://localhost:3000/api/player";
    } else if (user.role === "represented") {
      url = "http://localhost:3000/api/represented";
    }

    const res = await fetch(url, {
      method: "PUT",
      body: JSON.stringify(info),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    
    const data = await res.json();
    if (res.ok) {
      console.log(data);
    } else {
      const e = data.error;
      setError(e);
    }
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        {user.role === "represented" && (
          <fieldset>
            <legend>Familiar/representante</legend>
            <label>
              Dirección:
              <input
                onChange={handleInput}
                name="direccion_familia"
                maxlength="120"
                value={info.direccion_familia}
                placeholder={user.direccion_familia}
              ></input>
            </label>
            <br />
            <label>
              Email:
              <input
                onChange={handleInput}
                type="email"
                name="email_familia"
                value={info.email_familia}
                placeholder={user.email_familia}
              ></input>
            </label>
            <label>
              Teléfono:
              <input
                onChange={handleInput}
                name="telefono_familia"
                value={info.telefono_familia}
                placeholder={user.telefono_familia}
              ></input>
            </label>
          </fieldset>
        )}

        {(user.role === "player" || user.role === "represented") && (
          <fieldset>
            <legend>Jugador </legend>
            <label>
              Username:
              <input
                onChange={handleInput}
                name="username"
                maxlength="20"
                value={info.username}
                placeholder={user.username}
              ></input>
            </label>
            <br />
            <label>
              Dirección:
              <input
                onChange={handleInput}
                name="direccion"
                maxlength="120"
                value={info.direccion}
                placeholder={user.direccion}
              ></input>
            </label>
            <br />
            <label>
              Email:
              <input
                onChange={handleInput}
                type="email"
                name="email"
                value={info.email}
                placeholder={user.email}
              ></input>
            </label>
            <br />
            <label>
              Teléfono:
              <input
                onChange={handleInput}
                name="telefono"
                value={info.telefono}
                placeholder={user.telefono}
              ></input>
            </label>
            <label>
              Equipo:
              <input
                onChange={handleInput}
                name="equipo"
                maxlength="40"
                value={info.equipo}
                placeholder={user.team}
              ></input>
            </label>
            <br />
            <label>
              Pierna:
              <select onChange={handleInput} name="pierna" value={info.pierna}>
                <option value={user.pierna}>{user.pierna}</option>
                <option value="diestra">Diestra</option>
                <option value="zurda">Zurda</option>
                <option value="ambas">Ambas</option>
              </select>
            </label>
            <label>
              Posición:
              <select
                onChange={handleInput}
                name="posicion"
                value={info.posicion}
              >
                <option value={user.posicion}>{user.posicion}</option>
                <option value="portero">Portero</option>
                <option value="defensa">Defensa</option>
                <option value="medio">Medio</option>
                <option value="delantero">Delantero</option>
              </select>
            </label>
            <br />
            <button>Aceptar</button>
          </fieldset>
        )}
        {user.role === "scout" && (
          <fieldset>
            <legend>Ojeador</legend>
            <label>
              Username:
              <input
                onChange={handleInput}
                name="username"
                maxlength="20"
                value={info.username}
                placeholder={user.username}
              ></input>
            </label>
            <label>
              Dirección:
              <input
                onChange={handleInput}
                name="direccion"
                maxlength="120"
                value={info.direccion}
                placeholder={user.direccion}
              ></input>
            </label>
            <label>
              Email:
              <input
                onChange={handleInput}
                type="email"
                name="email"
                value={info.email}
                placeholder={user.email}
              ></input>
            </label>
            <label>
              Teléfono:
              <input
                onChange={handleInput}
                name="telefono"
                value={info.telefono}
                placeholder={user.telefono}
              ></input>
            </label>
            <label>
              Equipo:
              <input
                onChange={handleInput}
                name="equipo"
                maxlength="40"
                value={info.equipo}
                placeholder={user.team}
              ></input>
            </label>
            <button>Aceptar</button>
          </fieldset>
        )}
      </form>
      {error && <div>{error}</div>}
    </div>
  );
};

export default InfoChange;
