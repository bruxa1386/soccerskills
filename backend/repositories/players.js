const { getConnection } = require("../database");

const getForId = async (id) => {
  const connection = await getConnection();
  const query = `SELECT id, nombre, apellidos, email, equipo, id_familia,
    posicion, pierna, imagen, TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad
    FROM jugadores WHERE id = ?;`;
  const [rows] = await connection.query(query, [id]);
  return rows[0];
};

const playerExists = async (username) => {
  const connection = await getConnection();
  const [rows] = await connection.query(`SELECT * FROM jugadores
    WHERE username = '${username}';`);
  return rows[0] ? true : false;
};

const getForUsernamePlayer = async (username) => {
  const connection = await getConnection();
  const query = `SELECT id, username, password, nombre, apellidos,
  TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad, pierna, posicion, equipo,
  role, imagen, email, direccion, telefono, id_familia
  FROM jugadores WHERE username = ?;`;
  const [rows] = await connection.query(query, [username]);
  return rows[0];
};

const getAllPlayers = async () => {
  const connection = await getConnection();
  const [rows] =
    await connection.query(`SELECT id, username, nombre, apellidos, TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad,
    pierna, posicion, equipo, imagen FROM jugadores WHERE role = 'player' OR role = 'represented';`);
  return rows;
};

const postNewPlayer = async (user, password) => {
  const connection = await getConnection();
  const query = `INSERT INTO jugadores(username, password, nombre, apellidos,
        fecha_nacimiento, direccion, dni, email, telefono, posicion, pierna, equipo)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;

  await connection.query(query, [
    user.username,
    password,
    user.nombre,
    user.apellidos,
    user.fecha_nacimiento,
    user.direccion,
    user.dni,
    user.email,
    user.telefono,
    user.posicion,
    user.pierna,
    user.equipo,
  ]);
};

const postNewPlayerWithFamily = async (user, password, id_familia) => {
  const connection = await getConnection();
  const query = `INSERT INTO jugadores(username, password, nombre, apellidos,
        fecha_nacimiento, direccion, dni, email, telefono, posicion, pierna, equipo, role, id_familia)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;

  await connection.query(query, [
    user.username,
    password,
    user.nombre,
    user.apellidos,
    user.fecha_nacimiento,
    user.direccion,
    user.dni,
    user.email,
    user.telefono,
    user.posicion,
    user.pierna,
    user.equipo,
    "represented",
    id_familia,
  ]);
};
const putInfoPlayer = async (user, auth) => {
  const connection = await getConnection();
  let data = [];
  if (!user.username) {
    data = [...data, auth.username];
  } else {
    data = [...data, user.username];
  }
  if (!user.direccion) {
    data = [...data, auth.direccion];
  } else {
    data = [...data, user.direccion];
  }
  if (!user.email) {
    data = [...data, auth.email];
  } else {
    data = [...data, user.email];
  }
  if (!user.telefono) {
    data = [...data, auth.telefono];
  } else {
    data = [...data, user.telefono];
  }
  if (!user.posicion) {
    data = [...data, auth.posicion];
  } else {
    data = [...data, user.posicion];
  }
  if (!user.pierna) {
    data = [...data, auth.pierna];
  } else {
    data = [...data, user.pierna];
  }
  if (!user.equipo) {
    if (!auth.team) {
      auth.team = "";
    }
    data = [...data, auth.team];
  } else {
    data = [...data, user.equipo];
  }
  const query = `UPDATE jugadores SET username = ?,
    direccion = ?, email = ?, telefono = ?, posicion = ?,
    pierna = ?, equipo = ? WHERE (id = ?)`;
  await connection.query(query, [...data, auth.id]);
};

const putInfoRepresented = async (user, auth) => {
  const connection = await getConnection();
  let data = [];
  if (!user.username) {
    data = [...data, auth.username];
  } else {
    data = [...data, user.username];
  }
  if (!user.direccion) {
    data = [...data, auth.direccion];
  } else {
    data = [...data, user.direccion];
  }
  if (!user.email) {
    data = [...data, auth.email];
  } else {
    data = [...data, user.email];
  }
  if (!user.telefono) {
    data = [...data, auth.telefono];
  } else {
    data = [...data, user.telefono];
  }
  if (!user.posicion) {
    data = [...data, auth.posicion];
  } else {
    data = [...data, user.posicion];
  }
  if (!user.pierna) {
    data = [...data, auth.pierna];
  } else {
    data = [...data, user.pierna];
  }
  if (!user.equipo) {
    data = [...data, auth.team];
  } else {
    data = [...data, user.equipo];
  }

  let data2 = [];

  if (!user.direccion_familia) {
    data2 = [...data2, auth.direccion_familia];
  } else {
    data2 = [...data2, user.direccion_familia];
  }
  if (!user.email_familia) {
    data2 = [...data2, auth.email_familia];
  } else {
    data2 = [...data2, user.email_familia];
  }
  if (!user.telefono_familia) {
    data2 = [...data2, auth.telefono_familia];
  } else {
    data2 = [...data2, user.telefono_familia];
  }
  const query = `UPDATE jugadores SET username = ?,
    direccion = ?, email = ?, telefono = ?, posicion = ?,
    pierna = ?, equipo = ? WHERE (id = ?)`;
  await connection.query(query, [...data, auth.id]);

  const query2 = `UPDATE familias SET direccion = ?,
  email = ?, telefono = ? WHERE (id = ?)`;
  await connection.query(query2, [...data2, auth.id_familia]);
};

const putPasswordPlayer = async (password, id) => {
  const connection = await getConnection();
  const query = `UPDATE jugadores SET password = ? WHERE (id= ?)`;

  await connection.query(query, [password, id]);
};

const postImage = async (image, id) => {
  const connection = await getConnection();
  const query = `UPDATE jugadores SET imagen = ? WHERE (id= ?)`;
  await connection.query(query, [image, id]);
};

const postVideo = async (video, descripcion, id) => {
  const connection = await getConnection();
  const query = `INSERT INTO videos (video, descripcion, id_jugador)
    VALUES (?, ?, ?);`;
  await connection.query(query, [video, descripcion, id]);
};

const getContrats = async (id) => {
  const connection = await getConnection();
  const query = `SELECT C.id, C.estado, C.tiempo_contrato, C.clausula, C.fecha_inicio, C.salario,
    O.equipo, J.imagen, J.nombre, J.apellidos, J.posicion, J.pierna, O.Id as id_ojeador,
    J.equipo as equipo_jugador, J.id as id_jugador, O.imagen AS imagen_ojeador,
    O.nombre As nombre_ojeador, O.apellidos AS apellidos_ojeador, O.equipo AS equipo_ojeador,
    TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad
    FROM contratos C LEFT JOIN ojeadores O ON C.id_ojeador = O.id
    LEFT JOIN jugadores J ON J.id = C.id_jugador
    WHERE C.id_jugador=? ORDER BY C.id DESC;`;
  const [rows] = await connection.query(query, [id]);
  return rows;
};

const getPlayersOlderThan = async (age) => {
  const connection = await getConnection();
  const query = `SELECT J.nombre, J.apellidos, J.id, TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad
    FROM jugadores J WHERE TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) > ?;`;
  const [rows] = await connection.query(query, age);
  return rows;
};

const getPlayersUnder = async (age) => {
  const connection = await getConnection();
  const query = `SELECT J.nombre, J.apellidos, J.id, TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad
    FROM jugadores J WHERE TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) < ?;`;
  const [rows] = await connection.query(query, age);
  return rows;
};

const getVideos = async (id) => {
  const connection = await getConnection();
  const query = `SELECT id, id_jugador, descripcion, video FROM ojeadores.videos WHERE id_jugador = ?;`;
  const [rows] = await connection.query(query, id);
  return rows;
};

const delVideos = async (id) => {
  const connection = await getConnection();
  const query = `DELETE FROM videos WHERE id=?;`;
  const [rows] = await connection.query(query, id);
  return rows;
};

const getVideosByIdLimited = async (id) => {
  const connection = await getConnection();
  const query = `SELECT id, id_jugador, descripcion, video FROM ojeadores.videos WHERE id_jugador = ? LIMIT 3;`;
  const [rows] = await connection.query(query, id);
  return rows;
};

const getVideosByIdProfileCard = async (id) => {
  const connection = await getConnection();
  const query = `SELECT id, id_jugador, descripcion, video FROM ojeadores.videos WHERE id_jugador = ?;`;
  const [rows] = await connection.query(query, id);
  return rows;
};

module.exports = {
  getForId,
  getForUsernamePlayer,
  getAllPlayers,
  postNewPlayer,
  putInfoPlayer,
  putPasswordPlayer,
  postImage,
  playerExists,
  getContrats,
  getPlayersOlderThan,
  getPlayersUnder,
  getVideos,
  postNewPlayerWithFamily,
  postVideo,
  getVideosByIdLimited,
  getVideosByIdProfileCard,
  putInfoRepresented,
  delVideos,
};
