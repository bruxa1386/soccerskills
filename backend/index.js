const Joi = require("joi");
const { format, subYears } = require("date-fns");
const { searchContrat } = require("./repositories/contrats");

const today = new Date();
const maxAllowedDate = format(subYears(today, 18), "yyyy-MM-dd");
console.log(maxAllowedDate);

const v = Joi.date().max(maxAllowedDate).required().validate("2006-01-01");

console.log(v);

const a = async () => {
  const a = await searchContrat(1, 1, "cancelado");
  console.log(a);
};

a();
