const express = require("express");
const expressFileUp = require("express-fileupload");
require("dotenv").config();
const cors = require("cors");

const {
  playerForID,
  createPlayer,
  updateInfoPlayer,
  changePasswordPlayer,
  updateImage,
  playerContrats,
  playerVideos,
  getPlayers,
  updateVideo,
  playerVideosById,
  playerVideosByIdProfileCard,
  updateInfoRepresented,
  deleteVideos,
} = require("./controllers/playersControllers");
const {
  checkRegisterPlayer,
  checkPutPlayer,
  checkPutRepresented,
} = require("./validators/playersValidations");
const {
  checkRegisterScout,
  checkPutScout,
} = require("./validators/scoutsValidations");
const {
  scoreVideos,
  getAllVideos,
} = require("./controllers/videosControllers");
const {
  createScout,
  updateInfoScout,
  changePasswordScout,
  favsScout,
  updateImageScout,
  scoutContrats,
  addFavScout,
  favPlayerScout,
  putOrDelFavScout,
  getScouts,
} = require("./controllers/scoutControllers");
const {
  login,
  validateAuthorization,
  checkIsAdmin,
} = require("./controllers/authController");
const {
  checkLogin,
  checkID,
  checkPassword,
} = require("./validators/authValidation");
const { registerMail, contractMail } = require("./sendMail");
const { checkSendContract } = require("./validators/contratsValidations");
const {
  newContrat,
  acceptOrRejectContract,
  contractForId,
} = require("./controllers/contratsControllers");
const search = require("./controllers/search");
const { scoutVoteVideo } = require("./controllers/punctuationControllers");
const { deleteUser } = require("./controllers/adminControllers");
const { getAllPlayers, getVideos } = require("./repositories/players");
const { checkRegisterFamily } = require("./validators/familyValidations");
const { createFamily } = require("./controllers/familyControllers");

const app = express();
app.use(express.json());
app.use(expressFileUp());
app.use("/static", express.static("static"));
app.use(cors());

//usuarios no logueados
app.get("/api/players/explore", search);
app.get("/api/players", getPlayers);

app.post("/api/createplayer", checkRegisterPlayer, createPlayer, registerMail);
app.post("/api/createscout", checkRegisterScout, createScout, registerMail);
app.post("/api/createfamily", checkRegisterFamily, createFamily, registerMail);
app.post("/api/login", checkLogin, login);

app.get("/api/scorevideos", scoreVideos);
app.get("/api/videos", getAllVideos);
app.get("/api/videos/:id_jugador", playerVideosById);
app.get("/api/playerprofile/:id", checkID, playerForID);
app.get("/api/player/videos/:id_jugador", playerVideosByIdProfileCard);

//jugadores logueados
app.put("/api/player", validateAuthorization, checkPutPlayer, updateInfoPlayer);
app.put(
  "/api/represented",
  validateAuthorization,
  checkPutRepresented,
  updateInfoRepresented
);

app.put(
  "/api/player/pass",
  validateAuthorization,
  checkPassword,
  changePasswordPlayer
);
app.put(
  "/api/response/contrat/:id",
  checkID,
  validateAuthorization,
  acceptOrRejectContract
);

app.post("/api/player/image", validateAuthorization, updateImage);
app.post("/api/player/uploadVideo", validateAuthorization, updateVideo);

app.get("/api/player/contracts", validateAuthorization, playerContrats);
app.get("/api/player/videos", validateAuthorization, playerVideos);
app.get("/api/contract/:id", checkID, contractForId);

app.delete("/api/player/delete/video/:id", validateAuthorization, deleteVideos);


//ojeadores logueados
app.put("/api/scout", validateAuthorization, checkPutScout, updateInfoScout);
app.put(
  "/api/scout/pass",
  validateAuthorization,
  checkPassword,
  changePasswordScout
);

app.get("/api/scout/fav", validateAuthorization, favsScout);
app.get(
  "/api/fav/:id_jugador/scout/:id",
  validateAuthorization,
  favPlayerScout
);
app.get("/api/scout/contracts", validateAuthorization, scoutContrats);

app.put("/api/scout/fav", validateAuthorization, putOrDelFavScout);

app.post(
  "/api/scout/:id_scout/player/:id_player",
  validateAuthorization,
  addFavScout
);
app.post(
  "/api/scout/player/:id",
  checkID,
  validateAuthorization,
  checkSendContract,
  newContrat,
  contractMail
);
app.post(
  "/api/scout/video/:id",
  checkID,
  validateAuthorization,
  scoutVoteVideo
);
app.post("/api/scout/image", validateAuthorization, updateImageScout);
//admin
app.delete("/api/admin/del", validateAuthorization, checkIsAdmin, deleteUser);
app.get("/api/scouts", getScouts);

app.listen(process.env.PORT);
