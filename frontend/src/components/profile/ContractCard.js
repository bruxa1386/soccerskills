import { useHistory } from "react-router-dom";
import { useUser } from "../UserContext";
import "./ContractCard.css";

const ContractCard = ({ contract }) => {
  const [user] = useUser();
  const history = useHistory();

  const {
    id,
    id_jugador,
    estado,
    salario,
    clausula,
    imagen,
    nombre,
    apellidos,
    edad,
    equipo_jugador,
    imagen_ojeador,
    nombre_ojeador,
    apellidos_ojeador,
    equipo_ojeador,
  } = contract;
  return (
    <div className="contract-card">
      {user.role === "scout" && (
        <div className="card-contract-info">
          <div
            to={`/player/${id_jugador}`}
            className="image"
            style={{ backgroundImage: `url(${imagen})` }}
          />
          <div className="contract-player-info">
            <p>{nombre}</p>
            <p>{apellidos}</p>
            <p>{edad}</p>
            <p>{equipo_jugador}</p>
          </div>
        </div>
      )}
      {(user.role === "player" || user.role === "represented") && (
        <div className="card-contract-info">
          <div
            to={`/scout/${id_jugador}`}
            className="image"
            style={{ backgroundImage: `url(${imagen_ojeador})` }}
          />
          <div className="contract-player-info">
            <p>{nombre_ojeador}</p>
            <p>{apellidos_ojeador}</p>
            <p>{equipo_ojeador}</p>
          </div>
        </div>
      )}
      <div className="contract-info">
        <p>{estado}</p>
        <p>Sueldo: {salario}€/t</p>
        <p>Claúsula: {clausula}€</p>
      </div>
      <button onClick={() => history.push(`/contract/${id}`)}>+ info</button>
    </div>
  );
};

export default ContractCard;
