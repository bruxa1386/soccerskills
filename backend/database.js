require("dotenv").config();

const mysql = require("mysql2/promise");

const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;

let pool;
async function getConnection() {
  if (!pool) {
    pool = await mysql.createPool({
      host: MYSQL_HOST,
      database: MYSQL_DATABASE,
      user: MYSQL_USER,
      password: MYSQL_PASSWORD,
    });
  }
  return pool;
}

module.exports = {
  getConnection,
};
