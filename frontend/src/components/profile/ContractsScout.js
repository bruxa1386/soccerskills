import useFetch from "../useFetch";
import ContractCard from "./ContractCard";
import "./ContractsScout.css";

const ContractsScout = () => {
  const contracts = useFetch("http://localhost:3000/api/scout/contracts");
  return (
    <div>
      <h3>contratos:</h3>
      <div className="contracts-scout-profile">
        {contracts &&
          contracts.map((contract) => <ContractCard contract={contract} />)}
      </div>
    </div>
  );
};

export default ContractsScout;
