const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { getFamilyForIdPlayer } = require("../repositories/family");
const { getForUsernamePlayer } = require("../repositories/players");
const { getForUsernameScout } = require("../repositories/scouts");

const login = async (req, res) => {
  try {
    let user = await getForUsernamePlayer(req.body.username);
    if (!user) {
      user = await getForUsernameScout(req.body.username);
    }
    if (!user) {
      const error = new Error("No existe el usuario");
      error.code = 404;
      throw error;
    }
    let familia = {};
    if (user.id_familia) {
      familia = await getFamilyForIdPlayer(user.id);
    }
    const isValidPassword = await bcrypt.compare(
      req.body.password,
      user.password
    );
    if (!isValidPassword) {
      const error = new Error("El password no es válido");
      error.code = 401;
      throw error;
    }
    let tokenPayload = {
      id: user.id,
      username: user.username,
      role: user.role,
      image: user.imagen,
      name: user.nombre,
      surname: user.apellidos,
      email: user.email,
      team: user.equipo,
      posicion: user.posicion,
      edad: user.edad,
      direccion: user.direccion,
      pierna: user.pierna,
      telefono: user.telefono,
    };

    if (familia) {
      tokenPayload = {
        ...tokenPayload,
        id_familia: familia.id,
        nombre_familia: familia.nombre,
        apellidos_familia: familia.apellidos,
        direccion_familia: familia.direccion,
        dni_familia: familia.dni,
        email_familia: familia.email,
        telefono_familia: familia.telefono,
      };
    }
    const token = jwt.sign(tokenPayload, process.env.SECRET, {
      expiresIn: "30d",
    });
    res.send({ token });
  } catch (err) {
    res.status(500);
    res.send({ error: err.message });
  }
};

const validateAuthorization = async (req, res, next) => {
  try {
    const { authorization } = req.headers;
    if (!authorization) {
      const error = new Error("Authorization header required");
      error.code = 401;
      throw error;
    }
    const token = authorization.split(" ")[1];

    const decodedToken = jwt.verify(token, process.env.SECRET);

    let user = await getForUsernamePlayer(decodedToken.username);
    if (!user) {
      user = await getForUsernameScout(decodedToken.username);
    }
    if (!user) {
      const error = new Error("Authorization header required");
      error.code = 401;
      throw error;
    }
    req.auth = decodedToken;

    next();
  } catch (err) {
    res.send({ error: err.message });
  }
};

const checkIsAdmin = async (req, res, next) => {
  try {
    let user = await getForUsernamePlayer(req.auth.username);
    if (!user) {
      user = await getForUsernameScout(req.auth.username);
    }
    if (user.role != "admin") {
      const error = new Error("You are not a admin");
      res.status(401);
      throw error;
    }
    next();
  } catch (err) {
    res.send({ error: err.message });
  }
};

module.exports = { login, validateAuthorization, checkIsAdmin };
