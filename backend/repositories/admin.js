const mysql = require("mysql2/promise");
const { getConnection } = require("../database");
const { getForUsernamePlayer } = require("./players");
const { getForUsernameScout } = require("./scouts");

const delByUsername = async (username) => {
  const connection = await getConnection();
  let user = await getForUsernamePlayer(username);
  if (user) {
    const query = `DELETE FROM jugadores WHERE username = ?`;
    await connection.query(query, [username]);
  } else {
    user = await getForUsernameScout(username);
    if (user) {
      const query = `DELETE FROM ojeadores WHERE username = ?`;
      await connection.query(query, [username]);
    }
  }
  if (!user) {
    const error = new Error("No existe el usuario");
    error.code = 404;
    throw error;
  }
};

module.exports = { delByUsername };
