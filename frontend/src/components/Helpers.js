export default function decodeTokenData(token) {
  if (!token) {
    return null;
  }
  const tokenPieces = token.split(".");
  const tokenBase64 = tokenPieces[1];
  const decodedToken = decodeURIComponent(
    atob(tokenBase64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );
  return JSON.parse(decodedToken);
}

export function getVideoId(url) {
  const regExp =
    /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/;
  return url.match(regExp)[1];
}
