const bcrypt = require("bcryptjs");
const { v4: uuidv4 } = require("uuid");

const { playerExists } = require("../repositories/players");
const {
  postNewScout,
  putInfoScout,
  putPasswordScout,
  getFavScout,
  getForIdScout,
  postImageScout,
  getContratsScout,
  getFavScoutPlayer,
  newFavScout,
  changeFavScout,
  getAllScouts,
} = require("../repositories/scouts");


const getScouts = async (req, res, next) => {
  try {
    let users = [];

    users = await getAllScouts();

    if (users.length != 0) {
      res.send(users);
    } else {
      res.status(400);
      res.send("Bad request");
    }
  } catch (err) {
    res.status = 500;
    res.send({ error: err.message });
  }
  next();
};

const createScout = async (req, res, next) => {
  try {
    const passwordHash = await bcrypt.hash(req.body.password, 10);
    const user = await playerExists(req.body.username);
    if (user) {
      throw new Error("Ya existe un usuario con ese username");
    } else {
      await postNewScout(req.body, passwordHash);
      res.status(201);
      res.send(req.body);
    }
    next();
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const updateInfoScout = async (req, res) => {
  try {
    putInfoScout(req.body, req.auth);
    res.status(202);
    res.send(req.body);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const changePasswordScout = async (req, res) => {
  try {
    const passwordHash = await bcrypt.hash(req.body.password, 10);
    putPasswordScout(passwordHash, req.auth.id);
    res.status(202);
    res.send(req.body);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};
const favsScout = async (req, res) => {
  try {
    const favoritos = await getFavScout(req.auth.id);
    res.send(favoritos);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const favPlayerScout = async (req, res) => {
  try {
    const favoritos = await getFavScoutPlayer(req.params);
    res.send(favoritos);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const addFavScout = async (req, res) => {
  try {
    const favoritos = await newFavScout(
      req.params.id_player,
      req.params.id_scout
    );
    res.send(favoritos);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const putOrDelFavScout = async (req, res) => {
  try {
    const favoritos = await changeFavScout(req.body);
    res.send(favoritos);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const updateImageScout = async (req, res) => {
  try {
    let extension = req.files.imagen.name.split(".");
    const user = await getForIdScout(req.auth.id);
    if (user) {
      const archivo = uuidv4() + "." + extension[extension.length - 1];
      const ruta = __dirname + "/../static/profileimages/" + archivo;
      await req.files.imagen.mv(ruta);
      const rutaEstatica = `http://localhost:3000/static/profileimages/${archivo}`;
      postImageScout(rutaEstatica, req.auth.id);
      res.status(201);
      res.send({ message: "foto subida", ruta: rutaEstatica });
    } else {
      res.status(404);
      res.send("no existe el usuario");
    }
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const scoutContrats = async (req, res) => {
  try {
    const contrats = await getContratsScout(req.auth.id);
    res.send(contrats);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

module.exports = {
  createScout,
  updateInfoScout,
  changePasswordScout,
  favsScout,
  updateImageScout,
  scoutContrats,
  addFavScout,
  favPlayerScout,
  putOrDelFavScout,
  getScouts
};
