const {
  postNewFamily,
  familyForDni,
  deleteFamilyForId,
  getFamilyForIdPlayer,
} = require("../repositories/family");
const {
  playerExists,
  postNewPlayerWithFamily,
} = require("../repositories/players");
const { scoutExists } = require("../repositories/scouts");
const bcrypt = require("bcryptjs");

const createFamily = async (req, res, next) => {
  try {
    const passwordHash = await bcrypt.hash(req.body.password, 10);
    let user = await scoutExists(req.body.username);
    if (!user) {
      user = await playerExists(req.body.username);
    }
    if (user) {
      throw new Error("Ya existe un usuario con ese username");
    } else {
      await postNewFamily(req.body);
      const family = await familyForDni(req.body.dni_familiar);
      if (family) {
        await postNewPlayerWithFamily(req.body, passwordHash, family.id);
        res.status(201);
        res.send(req.body);
      } else {
        throw new Error("No se ha creado familia/representante");
      }
    }
    next();
  } catch (err) {
    const family = await familyForDni(req.body.dni_familiar);
    if (family) {
      deleteFamilyForId(family.id);
    }
    res.status(500).send({ error: err.message });
  }
};

module.exports = { createFamily };
