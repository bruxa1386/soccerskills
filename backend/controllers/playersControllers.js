const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
const {
  getForId,
  getAllPlayers,
  postNewPlayer,
  putInfoPlayer,
  putPasswordPlayer,
  postImage,
  getPlayersUnder,
  getPlayersOlderThan,
  getVideos,
  getContrats,
  postVideo,
  getVideosByIdLimited,
  getVideosByIdProfileCard,
  putInfoRepresented,
  delVideos,
} = require("../repositories/players");
const { scoutExists } = require("../repositories/scouts");

const getPlayers = async (req, res, next) => {
  try {
    const users = await getAllPlayers();

    if (users.length != 0) {
      res.send(users);
    } else {
      res.status(400);
      res.send("Bad request");
    }
  } catch (err) {
    res.status = 500;
    res.send({ error: err.message });
  }
  next();
};

const playerForID = async (req, res) => {
  try {
    const user = await getForId(req.params.id);
    if (!user) {
      res.status(404);
      throw new Error("No existe un usuario con ese id");
    } else {
      res.send(user);
    }
  } catch (err) {
    res.send({ error: err.message });
  }
};

const createPlayer = async (req, res, next) => {
  try {
    const passwordHash = await bcrypt.hash(req.body.password, 10);
    const user = await scoutExists(req.body.username);
    if (user) {
      throw new Error("Ya existe un usuario con ese username");
    } else {
      await postNewPlayer(req.body, passwordHash);
      res.status(201);
      res.send(req.body);
    }
    next();
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
};

const updateInfoPlayer = async (req, res) => {
  try {
    putInfoPlayer(req.body, req.auth);
    res.status(202);
    res.send(req.body);
  } catch (err) {
    res.send({ error: err.message });
  }
};

const updateInfoRepresented = async (req, res) => {
  try {
    putInfoRepresented(req.body, req.auth);
    res.status(202);
    res.send(req.body);
  } catch (err) {
    res.send({ error: err.message });
  }
};

const changePasswordPlayer = async (req, res) => {
  try {
    const passwordHash = await bcrypt.hash(req.body.password, 10);
    putPasswordPlayer(passwordHash, req.auth.id);
    res.status(202);
    res.send(req.body);
  } catch (err) {
    res.send({ error: err.message });
  }
};

const updateImage = async (req, res) => {
  try {
    let extension = req.files.imagen.name.split(".");
    const user = await getForId(req.auth.id);
    if (user) {
      const archivo = uuidv4() + "." + extension[extension.length - 1];
      const ruta = __dirname + "/../static/profileimages/" + archivo;
      await req.files.imagen.mv(ruta);
      const rutaEstatica = `http://localhost:3000/static/profileimages/${archivo}`;
      postImage(rutaEstatica, req.auth.id);
      res.status(201);
      res.send({ message: "foto subida", ruta: rutaEstatica });
    } else {
      res.status(404);
      res.send("no existe el usuario");
    }
  } catch (err) {
    res.send({ error: err.message });
  }
};

const updateVideo = async (req, res) => {
  try {
    const user = await getForId(req.auth.id);
    if (user) {
      postVideo(req.body.video, req.body.descripcion, req.auth.id);
      res.status(201);
      res.send({ message: "video subido" });
    } else {
      res.status(404);
      res.send({ error: "no existe el usuario" });
    }
  } catch (err) {
    res.send({ error: err.message });
  }
};

const playerContrats = async (req, res) => {
  try {
    const contrats = await getContrats(req.auth.id);
    res.send(contrats);
  } catch (err) {
    res.send({ error: err.message });
  }
};

const playerVideos = async (req, res) => {
  try {
    const videos = await getVideos(req.auth.id);
    res.send(videos);
  } catch (err) {
    res.send({ error: err.message });
  }
};

const deleteVideos = async (req, res) => {
  try {
    const videos = await delVideos(req.params.id);
    res.send(videos);
  } catch (err) {
    res.send({ error: err.message });
  }
};

const playerVideosById = async (req, res) => {
  try {
    const videos = await getVideosByIdLimited(req.params.id_jugador);
    res.send(videos);
  } catch (err) {
    res.send({ error: err.message });
  }
};

const playerVideosByIdProfileCard = async (req, res) => {
  try {
    const videos = await getVideosByIdProfileCard(req.params.id_jugador);
    res.send(videos);
  } catch (err) {
    res.send({ error: err.message });
  }
};

module.exports = {
  playerForID,
  createPlayer,
  updateInfoPlayer,
  changePasswordPlayer,
  updateImage,
  playerContrats,
  playerVideos,
  getPlayers,
  updateVideo,
  playerVideosById,
  playerVideosByIdProfileCard,
  updateInfoRepresented,
  deleteVideos,
};
