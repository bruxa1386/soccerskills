import { useState } from "react";
import { useHistory } from "react-router-dom";

const ScoutRegister = () => {
  const [info, setInfo] = useState({});
  const [error, setError] = useState(false);
  const history = useHistory();

  const handleInput = (e) =>
    setInfo({ ...info, [e.target.name]: e.target.value });

  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3000/api/createscout", {
      method: "POST",
      body: JSON.stringify(info),
      headers: { "Content-Type": "application/json" },
    });
    const data = await res.json();
    if (res.ok) {
      history.push("/");
    } else {
      const e = data.error;
      setError(e);
    }
  };

  return (
    <div classname="register-form">
      <form onSubmit={handleSubmit} classname="register-form">
        <fieldset>
          <legend>Ojeador</legend>
          <label>
            Nombre:
            <input
              onChange={handleInput}
              name="nombre"
              maxlength="20"
              value={info.nombre}
              required
            ></input>
          </label>
          <label>
            Apellidos:
            <input
              onChange={handleInput}
              name="apellidos"
              maxlength="100"
              value={info.apellidos}
              required
            ></input>
          </label>
          <label>
            Username:
            <input
              onChange={handleInput}
              name="username"
              maxlength="20"
              value={info.username}
              required
            ></input>
          </label>
          <label>
            Password:
            <input
              onChange={handleInput}
              type="password"
              maxlength="40"
              minlength="5"
              name="password"
              value={info.password}
              required
            ></input>
          </label>
          <label>
            Dirección:
            <input
              onChange={handleInput}
              name="direccion"
              maxlength="120"
              value={info.direccion}
            ></input>
          </label>
          <label>
            DNI:
            <input
              onChange={handleInput}
              name="dni"
              maxlength="15"
              value={info.dni}
            ></input>
          </label>
          <label>
            Email:
            <input
              onChange={handleInput}
              type="email"
              name="email"
              value={info.email}
              required
            ></input>
          </label>
          <label>
            Teléfono:
            <input
              onChange={handleInput}
              name="telefono"
              value={info.telefono}
            ></input>
          </label>
          <label>
            Equipo:
            <input
              onChange={handleInput}
              name="equipo"
              maxlength="40"
              value={info.equipo}
            ></input>
          </label>
          <button>Regístrate</button>
        </fieldset>
      </form>
      {error && <div className="error-form">{error}</div>}
    </div>
  );
};

export default ScoutRegister;
