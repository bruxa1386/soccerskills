import "./Header.css";
import { NavLink, useHistory } from "react-router-dom";
import { useUser } from "../UserContext";

const Header = () => {
  const [user, setUser] = useUser();
  const history = useHistory();
  const handleSubmit = () => {
    setUser();
    history.push("/");
  };
  return (
    <header className="header">
      {!user && (
        <NavLink activeClassName="active" to="/" exact>
          <div className="logo-container" />{" "}
        </NavLink>
      )}
      {user && (
        <NavLink activeClassName="active" to="/profile" exact>
          <div className="logo-container" />{" "}
        </NavLink>
      )}
      <nav className="nav">
        <div>
          {!user && (
            <NavLink activeClassName="active" to="/" exact>
              Portada
            </NavLink>
          )}
          <NavLink activeClassName="active" to="/players" exact>
            Jugadores
          </NavLink>
          <NavLink activeClassName="active" to="/videos">
            Videos
          </NavLink>
          {!user && (
            <NavLink activeClassName="active" to="/login">
              <button className="login">Login</button>
            </NavLink>
          )}
        </div>
        {user && (
          <div className="user">
            <NavLink to="/profile">
              <div
                activeClassName="active-user"
                className="avatar"
                style={{ backgroundImage: `url(${user.image})` }}
              />
            </NavLink>
            <div>
              <p>{user.username}</p>
              <button className="logout" onClick={handleSubmit}>
                Logout
              </button>
            </div>
          </div>
        )}
      </nav>
    </header>
  );
};

export default Header;
