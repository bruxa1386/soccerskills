const Joi = require("joi");
const { format, subYears } = require("date-fns");

const checkRegisterPlayer = (req, res, next) => {
  const today = new Date();
  const maxAllowedDate = format(subYears(today, 18), "yyyy-MM-dd");
  const schema = Joi.object({
    username: Joi.string().max(20).required(),
    password: Joi.string().min(5).max(40).required(),
    nombre: Joi.string().max(20).required(),
    apellidos: Joi.string().max(100).required(),
    fecha_nacimiento: Joi.date().max(maxAllowedDate).required(),
    direccion: Joi.string().max(120),
    dni: Joi.string().max(15).required(),
    email: Joi.string().email().required(),
    telefono: Joi.number(),
    posicion: Joi.string()
      .valid("portero", "defensa", "medio", "delantero")
      .required(),
    pierna: Joi.string().valid("diestra", "zurda", "ambas"),
    equipo: Joi.string().max(40),
  });
  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

const checkLoginPlayer = (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().max(20).required(),
    password: Joi.string().min(5).max(40).required(),
  });
  const validation = schema.validate(req.body);
  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

const checkPutPlayer = (req, res, next) => {
  let objeto = {};
  if (req.body.username) {
    objeto = { ...objeto, username: Joi.string().max(20).required() };
  }
  if (req.body.direccion) {
    objeto = { ...objeto, direccion: Joi.string().max(120) };
  }
  if (req.body.email) {
    objeto = { ...objeto, email: Joi.string().email().required() };
  }
  if (req.body.telefono) {
    objeto = { ...objeto, telefono: Joi.number() };
  }
  if (req.body.equipo) {
    objeto = { ...objeto, equipo: Joi.string().max(40).required() };
  }
  if (req.body.posicion) {
    objeto = {
      ...objeto,
      posicion: Joi.string()
        .valid("portero", "defensa", "medio", "delantero")
        .required(),
    };
  }
  if (req.body.pierna) {
    objeto = {
      ...objeto,
      pierna: Joi.string().valid("diestra", "zurda", "ambas"),
    };
  }
  const schema = Joi.object(objeto);
  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};
const checkPutRepresented = (req, res, next) => {
  let objeto = {};
  if (req.body.username) {
    objeto = { ...objeto, username: Joi.string().max(20).required() };
  }
  if (req.body.direccion) {
    objeto = { ...objeto, direccion: Joi.string().max(120) };
  }
  if (req.body.email) {
    objeto = { ...objeto, email: Joi.string().email() };
  }
  if (req.body.telefono) {
    objeto = { ...objeto, telefono: Joi.number() };
  }
  if (req.body.equipo) {
    objeto = { ...objeto, equipo: Joi.string().max(40).required() };
  }
  if (req.body.posicion) {
    objeto = {
      ...objeto,
      posicion: Joi.string()
        .valid("portero", "defensa", "medio", "delantero")
        .required(),
    };
  }
  if (req.body.pierna) {
    objeto = {
      ...objeto,
      pierna: Joi.string().valid("diestra", "zurda", "ambas"),
    };
  }
  if (req.body.direccion_familia) {
    objeto = { ...objeto, direccion_familia: Joi.string().max(120) };
  }
  if (req.body.email_familia) {
    objeto = { ...objeto, email_familia: Joi.string().email().required() };
  }
  if (req.body.telefono_familia) {
    objeto = { ...objeto, telefono_familia: Joi.number() };
  }
  const schema = Joi.object(objeto);
  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

module.exports = {
  checkRegisterPlayer,
  checkLoginPlayer,
  checkPutPlayer,
  checkPutRepresented,
};
