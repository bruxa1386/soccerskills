import { useState } from "react";
import { useHistory } from "react-router-dom";
import decodeTokenData from "../Helpers";
import "./Login.css";
import { useUser } from "../UserContext";

function Login() {
  const [user, setUser] = useUser();
  const [badLogin, setBadLogin] = useState(false);
  const [load, setLoad] = useState(false);
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoad(true);
    const res = await fetch("http://localhost:3000/api/login", {
      method: "POST",
      body: JSON.stringify({
        username: e.target.username.value,
        password: e.target.password.value,
      }),
      headers: { "Content-type": "application/json" },
    });
    if (res.ok) {
      const data = await res.json();
      let decodedUser = decodeTokenData(data.token);
      decodedUser = { ...decodedUser, token: data.token };
      setUser(decodedUser, data.token);
      history.push("/profile");
    } else {
      setUser();
      setBadLogin(true);
      setLoad(false);
    }
  };
  return (
    <form onSubmit={handleSubmit} className="form-login">
      <fieldset>
        <legend>Inicia sesión</legend>
        <label>
          Username:
          <input name="username" />
        </label>
        <label>
          Password:
          <input name="password" type="password" />
        </label>
        <button>Iniciar sesión</button>
        {badLogin && (
          <p className="incorrecto">Usuario o contraseña incorrectos</p>
        )}
      </fieldset>
    </form>
  );
}

export default Login;
