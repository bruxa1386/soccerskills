const mysql = require("mysql2/promise");
const { getConnection } = require("../database");
const { getForIdScout } = require("./scouts");

const createContrat = async (data) => {
  const connection = await getConnection();
  const contratos = await searchContrat(
    data.id_jugador,
    data.id_ojeador,
    "solicitado"
  );
  data.estado = "solicitado";
  if (contratos.length > 0) {
    throw new Error("Ya se ha enviado un contrato a este jugador");
  } else {
    const query = `INSERT INTO contratos(id_ojeador, id_jugador, salario,
            fecha_inicio, clausula, tiempo_contrato, estado)
           VALUES(?, ?, ?, ?, ?, ?, ?);`;
    await connection.query(query, [
      data.id_ojeador,
      data.id_jugador,
      data.salario,
      data.fecha_inicio,
      data.clausula,
      data.tiempo_contrato,
      data.estado,
    ]);
  }
};

const searchContrat = async (id_jugador, id_ojeador, estado) => {
  const connection = await getConnection();
  const query = `SELECT * FROM contratos WHERE id_jugador = ? AND id_ojeador = ? AND estado= ?;`;
  const [contratos] = await connection.query(query, [
    id_jugador,
    id_ojeador,
    estado,
  ]);
  return contratos;
};

const searchContratId = async (id) => {
  const connection = await getConnection();
  const query = `SELECT C.*, O.equipo, O.nombre, O.email, O.nombre, O.apellidos, O.imagen,
  J.nombre as nombre_jugador, J.equipo as equipo_jugador, J.apellidos as apellidos_jugador,
  J.email as email_jugador, F.email as email_familia
  FROM contratos C LEFT JOIN ojeadores O ON C.id_ojeador = O.id
  LEFT JOIN jugadores J ON C.id_jugador = J.id
  LEFT JOIN familias F ON F.id = J.id_familia
  WHERE C.id = ?;`;
  const [contrato] = await connection.query(query, [id]);
  return contrato;
};

const modificateContratState = async (id, estado, id_jugador) => {
  const connection = await getConnection();
  const [contrato] = await searchContratId(id);
  if (contrato) {
    const query = `UPDATE contratos SET estado = ? WHERE id = ?`;
    await connection.query(query, [estado, id]);
    if (estado === "confirmado") {
      const ojeador = await getForIdScout(contrato.id_ojeador);
      const query1 = `UPDATE jugadores SET equipo = ? WHERE id = ?`;
      await connection.query(query1, [ojeador.equipo, id_jugador]);
    } else if (estado === "cancelado") {
      const query1 = `UPDATE jugadores SET equipo = ? WHERE id = ?`;
      await connection.query(query1, [null, id_jugador]);
    }
  } else {
    throw new Error("No existe un contrato con ese ID");
  }
};

module.exports = {
  createContrat,
  modificateContratState,
  searchContrat,
  searchContratId,
};
