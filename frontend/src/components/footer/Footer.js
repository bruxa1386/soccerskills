import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer">
      <ul>
        <li>
          <a href="localhost">Contáctanos</a>
        </li>
        <li>
          <a href="localhost">Política de privacidad</a>
        </li>
      </ul>
      <p>
        Copyright © Soccer Skills. Todos los derechos reservados. Soccer Skills
        es una marca registrada.
      </p>
    </div>
  );
};

export default Footer;
