const mysql = require('mysql2/promise')
const { getConnection } = require('../database')

const addPunctuation = async (puntuacion, favorito, id_video, id_ojeador) => {
    const connection = await getConnection()
    const [exists] = await getPunctuationIdVideoAndIdscout(id_video, id_ojeador)
    if (exists) {
        query = (`UPDATE puntuaciones SET puntuacion = ?, favorito = ? WHERE id = ?`)
        const [row] = await connection.query(query, [puntuacion, favorito, exists.id])
        return row
    } else {
        query = (`INSERT INTO puntuaciones (puntuacion, favorito, id_video, id_ojeador)
        VALUES (?, ?, ?, ?)`)
        const [row] = await connection.query(query, [puntuacion, favorito, id_video, id_ojeador])
        return row
    }

}

const getPunctuationIdVideoAndIdscout = async (id_video, id_ojeador) => {
    const connection = await getConnection()
    query = (`SELECT * FROM puntuaciones WHERE id_video = ? AND id_ojeador = ?`)
    const [puntuaciones] = await connection.query(query, [id_video, id_ojeador])
    return puntuaciones
}



module.exports = { addPunctuation, getPunctuationIdVideoAndIdplayer: getPunctuationIdVideoAndIdscout }