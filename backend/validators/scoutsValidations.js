const Joi = require("joi");

const checkRegisterScout = (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().max(20).required(),
    password: Joi.string().min(5).max(40).required(),
    nombre: Joi.string().max(20).required(),
    apellidos: Joi.string().max(100).required(),
    direccion: Joi.string().max(120),
    dni: Joi.string().max(15).required(),
    email: Joi.string().email().required(),
    telefono: Joi.number(),
    equipo: Joi.string().max(40).required(),
  });
  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

const checkLoginScout = (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().max(20).required(),
    password: Joi.string().min(5).max(40).required(),
  });
  const validation = schema.validate(req.body);
  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

const checkPutScout = (req, res, next) => {
  let objeto = {};
  if (req.body.username) {
    objeto = { ...objeto, username: Joi.string().max(20).required() };
  }
  if (req.body.direccion) {
    objeto = { ...objeto, direccion: Joi.string().max(120) };
  }
  if (req.body.email) {
    objeto = { ...objeto, email: Joi.string().email().required() };
  }
  if (req.body.telefono) {
    objeto = { ...objeto, telefono: Joi.number() };
  }
  if (req.body.equipo) {
    objeto = { ...objeto, equipo: Joi.string().max(40).required() };
  }
  const schema = Joi.object(objeto);
  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

module.exports = { checkRegisterScout, checkLoginScout, checkPutScout };
