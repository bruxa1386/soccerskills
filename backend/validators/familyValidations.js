const Joi = require("joi");

const checkRegisterFamily = (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().max(20).required(),
    password: Joi.string().min(5).max(40).required(),
    nombre: Joi.string().max(20).required(),
    apellidos: Joi.string().max(100).required(),
    nombre_familiar: Joi.string().max(20).required(),
    apellidos_familiar: Joi.string().max(100).required(),
    fecha_nacimiento: Joi.date().required(),
    direccion: Joi.string().max(120),
    dni: Joi.string().max(15),
    email: Joi.string().email(),
    telefono: Joi.number(),
    direccion_familiar: Joi.string().max(120),
    dni_familiar: Joi.string().max(15).required(),
    email_familiar: Joi.string().email().required(),
    telefono_familiar: Joi.number(),
    posicion: Joi.string()
      .valid("portero", "defensa", "medio", "delantero")
      .required(),
    pierna: Joi.string().valid("diestra", "zurda", "ambas"),
    equipo: Joi.string().max(40),
  });
  const validation = schema.validate(req.body);

  if (validation.error) {
    res.status(400);
    res.send({ error: validation.error.message });
  } else {
    next();
  }
};

module.exports = { checkRegisterFamily };
