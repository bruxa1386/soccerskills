import useFetch from "../useFetch";
import { useUser } from "../UserContext";
import "./AdminPanel.css";

const AdminPanel = () => {
  const [user] = useUser();
  const token = user && user.token;

  const players = useFetch("http://localhost:3000/api/players");
  const scouts = useFetch("http://localhost:3000/api/scouts");

  const handleClick = async (user) => {
    const result = window.confirm(
      `Seguro que quieres eliminar a ${user.username}?`
    );
    if (result) {
      const data = { username: user.username };
      await fetch("http://localhost:3000/api/admin/del", {
        method: "DELETE",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      });
      window.location.reload();
    }
  };

  return (
    <div id="adminpanel">
      <div className="players-admin-container">
        Jugadores:
        <hr />
        {players &&
          players.map((player) => (
            <div key={player.id} classname="players-admin">
              <p>
                {player.id} / {player.username} / {player.nombre}{" "}
                {player.apellidos} / {player.edad} años{" "}
                <button onClick={() => handleClick(player)}>X</button>
              </p>
            </div>
          ))}{" "}
      </div>
      <div className="scouts-admin-container">
        Ojeadores:
        <hr />
        {scouts &&
          scouts.map((scout) => (
            <div key={scout.id} classname="scouts-admin">
              <p>
                {scout.id} / {scout.username} / {scout.nombre} {scout.apellidos}{" "}
                <button onClick={() => handleClick(scout)}>X</button>
              </p>
            </div>
          ))}{" "}
      </div>
    </div>
  );
};

export default AdminPanel;
