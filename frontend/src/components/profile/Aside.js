import { Link, useHistory } from "react-router-dom";
import { useUser } from "../UserContext";

const Aside = () => {
  const [user] = useUser();
  const history = useHistory();

  const handleClick = () => {
    history.push("/infoChange");
  };
  let rol = "";
  if (user.role === "player" || user.role === "represented") {
    rol = "jugador";
  } else if (user.role === "scout") {
    rol = "ojeador";
  }
  return (
    <aside className="aside">
      <Link to="/uploadImage">
        {" "}
        <div
          className="avatar"
          style={{ backgroundImage: `url(${user.image})` }}
        />
      </Link>
      <div className="datos-personales">
        <p>Nombre: {user.name}</p> <br />
        <p>Apellidos: {user.surname}</p> <br />
        <p>Dirección: {user.direccion}</p>
        <br />
        <p>Teléfono: {user.telefono}</p> <br />
        <p>Email: {user.email}</p> <br />
        <p>Equipo: {user.team}</p>{" "}
        {(user.role === "player" || user.role === "represented") && (
          <div>
            <br />
            <p>Posición: {user.posicion}</p>
            <p>Pierna: {user.pierna}</p>{" "}
          </div>
        )}
        <br />
        <br />
        Rol: {rol}
        <br />
        <br />
      </div>
      <button onClick={handleClick}>Modificar datos</button>
    </aside>
  );
};

export default Aside;
