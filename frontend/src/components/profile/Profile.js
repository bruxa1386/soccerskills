import { useUser } from "../UserContext";
import "./Profile.css";
import VideosJugador from "./VideosJugador";
import ContratosJugador from "./ContratosJugador";
import Aside from "./Aside";
import FavScout from "./FavScout";
import ContractsScout from "./ContractsScout";
import AdminPanel from "./AdminPanel";

const Profile = () => {
  const [user] = useUser();

  return (
    <div className="profile">
      {user && <Aside />}
      {(user.role === "player" || user.role === "represented") && (
        <div>
          <h3>Mis vídeos:</h3>
          <VideosJugador />
          <h3>Mis contratos:</h3>
          <ContratosJugador />
        </div>
      )}
      {user.role === "scout" && (
        <div>
          <FavScout />
          <ContractsScout />
        </div>
      )}
      {user.role === "admin" && <AdminPanel />}
    </div>
  );
};

export default Profile;
