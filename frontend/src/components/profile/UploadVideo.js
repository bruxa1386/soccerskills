import { useState } from "react";
import { useHistory } from "react-router";
import { getVideoId } from "../Helpers";
import { useUser } from "../UserContext";

const UploadVideo = () => {
  const [info, setInfo] = useState({});
  const [error, setError] = useState(false);
  const [user] = useUser();
  const history = useHistory();
  const token = user && user.token;

  const handleInput = (e) =>
    setInfo({ ...info, [e.target.name]: e.target.value });
  const handleInputUrl = (e) =>
    setInfo({ ...info, [e.target.name]: getVideoId(e.target.value) });

  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:3000/api/player/uploadVideo", {
      method: "POST",
      body: JSON.stringify(info),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    const data = await res.json();
    if (res && res.ok) {
      history.push("/profile");
    } else {
      const e = data.error;
      setError(e);
    }
  };
  return (
    <div>
      <form onSubmit={handleSubmit} className="form-upload-video">
        <fieldset>
        <legend>Sube un vídeo de youtube</legend>
          <label>
            Url:
            <input onChange={handleInputUrl} required name="video"></input>
          </label>
          <label>
            Descripción:
            <input onChange={handleInput} required name="descripcion"></input>
          </label>
          <br />
          <button>Subir</button>
        </fieldset>
      </form>
      {error && <div>{error}</div>}
    </div>
  );
};

export default UploadVideo;
