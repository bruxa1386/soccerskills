import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useUser } from "../UserContext";

const UploadImage = () => {
  const [file, setFile] = useState(null);
  const [error, setError] = useState(false);
  const [user, setUser] = useUser();
  const history = useHistory();
  const token = user && user.token;

  const selectedHandler = (e) => setFile(e.target.files[0]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!file) {
      alert("No has seleccionado una imagen!");
      return;
    }
    const formdata = new FormData();
    formdata.append("imagen", file);
    let url = "";
    if (user.role === "player" || user.role === "represented") {
      url = "http://localhost:3000/api/player/image";
    } else if (user.role === "scout") {
      url = "http://localhost:3000/api/scout/image";
    }

    const res = await fetch(url, {
      method: "POST",
      body: formdata,
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    const data = await res.json();
    if (res && res.ok) {
      setUser({ ...user, image: data.ruta });
      history.push("/profile");
    } else {
      const e = data.error;
      setError(e);
    }
  };
  return (
    <div>
      <form onSubmit={handleSubmit} type="POST">
        <label>
          Imagen:
          <input
            onChange={selectedHandler}
            required
            name="archivo"
            type="file"
          ></input>
        </label>
        <button>Subir</button>
      </form>
      {error && <div>{error}</div>}
    </div>
  );
};

export default UploadImage;
